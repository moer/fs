from sklearn import metrics
from sklearn.model_selection import train_test_split, cross_val_score, KFold, StratifiedKFold, ShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier
from solution import Solution

import lightgbm as lgb

class FsProblem:
    def __init__(self, typeOfAlgo, data, data_weight, qlearn, classifier='svc', cost=True):
        s_classifier = None
        if classifier == 'svc':
            s_classifier = SVC(gamma='auto')
        elif classifier == 'knn':
            s_classifier = KNeighborsClassifier(n_neighbors=1)
        elif classifier == 'forest':
            s_classifier = RandomForestClassifier(max_depth=2, n_estimators=10, random_state=0, n_jobs=-1)
        elif classifier == 'xgboost':
            s_classifier = XGBClassifier(objective='binary:logistic', 
                                         eval_metric='logloss', 
                                         use_label_encoder=False,
                                         learning_rate=0.1, 
                                         max_depth=2,
                                         n_estimators=8)
        elif classifier == 'light':
            s_classifier = lgb.LGBMClassifier(objective='binary', learning_rate=0.03, max_depth=10, random_state=0)


        assert(s_classifier is not None)

        self.cost = cost
        self.data = data
        self.data_weight = data_weight
        # The number of features is the size of the dataset - the 1 column of labels
        self.nb_attribs = len(self.data.columns)-1
        # We initilize the labels from the last column of the dataset
        self.outPuts = self.data.iloc[:, self.nb_attribs]
        self.ql = qlearn
        self.classifier = s_classifier
        self.typeOfAlgo = typeOfAlgo

    def evaluate2(self, solution):
        sol_list = Solution.sol_to_list(solution)
        if (len(sol_list) == 0):
            return 0

        df = self.data.iloc[:, sol_list]
        array = df.values
        X = array[:, 0:self.nb_attribs]

        # normalization
        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)

        Y = self.outPuts
        train_X, test_X, train_y, test_y = train_test_split(X, Y,
                                                            random_state=0,
                                                            test_size=0.1
                                                            )
        self.classifier.fit(train_X, train_y)
        predict = self.classifier.predict(test_X)
        return metrics.accuracy_score(predict, test_y)

    def evaluate(self, solution):
        sol_list = Solution.sol_to_list(solution)
        if (len(sol_list) == 0):
            return 0

        # For this function you need to put the indexes of features you picked
        df = self.data.iloc[:, sol_list]
        array = df.values
        X = array[:, 0:self.nb_attribs]

        # normalization
        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)

        Y = self.outPuts
        # Cross validation function
        cv = ShuffleSplit(n_splits=10, test_size=0.1, random_state=0)
        results = cross_val_score(
            self.classifier, X, Y, cv=cv, scoring='accuracy')
        #print("\n[Cross validation results]\n{0}".format(results))
        return results.mean()
