import argparse
from fs_data import FSData

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Q Learning and BSO.')

    parser.add_argument('--execution', required=True, type=int, default=1,
                        help='number of execution')
    parser.add_argument('--classifier', required=True, type=str, default='svc',
                        help='classification type')
    parser.add_argument('--cost', action='store_true')

    args = parser.parse_args()

    # RL

    alhpa = 0.1
    gamma = 0.99
    epsilon = 0.01

    # BSO

    flip = 5
    max_chance = 3
    bees_number = 10
    maxIterations = 10
    locIterations = 10

    # Test type

    typeOfAlgo = 1
    nbr_exec = int(args.execution)
    dataset = "Alizadeh"
    data_loc_path = "./datasets/"
    location = data_loc_path + dataset + ".csv"
    location_weight = data_loc_path + dataset + "_weight.csv"
    method = "qbso_simple"
    test_param = "rl"
    param = "gamma"
    val = str(locals()[param])
    classifier = str(args.classifier)
    using_cost = args.cost

    print('Running feature selection')
    instance = FSData(typeOfAlgo, location, location_weight, nbr_exec, method,
                      test_param, param, val, classifier, alhpa, gamma, epsilon, using_cost)
    instance.run(flip, max_chance, bees_number, maxIterations, locIterations)
